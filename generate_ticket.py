from io import BytesIO

from PIL import Image, ImageDraw, ImageFont
import requests



def generate_ticket(name, email):
    base = Image.open("card.jpg").convert("RGBA")
    font = ImageFont.truetype("Roboto-Regular.ttf", 15)

    draw = ImageDraw.Draw(base)
    draw.text((360, 130), name, font=font, fill=(0, 0, 0, 255))
    draw.text((440, 130), email, font=font, fill=(0, 0, 0, 255))

    url = f'https://api.adorable.io/avatars/100/{email}'
    image = requests.get(url)
    avatar_file_like = BytesIO(image.content)
    avatar = Image.open(avatar_file_like)
    base.paste(avatar, (500, 0))
    temp_file = BytesIO()
    base.save(temp_file, 'png')
    temp_file.seek(0)
    return temp_file

