import re

from generate_ticket import generate_ticket

re_name = re.compile(r'^[\w\-\s]{3,40}$')
re_mail = re.compile(r"\b[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+\b")


def handler_name(text, context):
    match = re.findall(re_name, text)
    if match:
        context['name'] = text
        return True
    else:
        return False


def handler_email(text, context):
    matches = re.findall(re_mail, text)
    if len(matches):
        context['email'] = matches[0]
        return True
    else:
        return False


def generate_ticket_handler(text, context):
    return generate_ticket(name=context['name'], email=context['email'])


INTENTS = [
    {
        'name': 'Дата проведения',
        'tokens': ('когда', "сколько", "дата", "дату",),
        'scenario:': None,
        'answer': "Конференция начнется 15го апреля, регистрация в 10 утра"
    },
    {
        'name': 'Место проведения',
        'tokens': ('где', "место", "локация", "адрес", 'метро'),
        'scenario:': None,
        'answer': "Конференция пройдет в павильоне 18Г в Экспоцентре"
    },
    {
        'name': 'Регистрация',
        'tokens': ('регист', "добав"),
        'scenario': 'registration',
        'answer': None
    },

]
SCENARIOS = {
    'registration': {
        'first_step': 'step1',
        'steps': {
            'step1': {
                'text': 'Чтобы зарегистрироваться, введите Ваше имя. Оно будет написано на бэйджике',
                'failure_text': 'Имя должно состоять из -30 букв и дефиса. Попробуйте еще раз',
                'handler': "handler_name",
                'next_step': 'step2'
            },
            'step2': {
                'text': 'Введите email. Мы отправим на него все данные',
                'failure_text': 'Во введенном адресе ошибка. Попробуйте еще раз',
                'handler': "handler_email",
                'next_step': 'step3'
            },
            'step3': {
                'text': 'Спасибо за регистрацию, {name}! Ваш билет ниже!',
                'image': 'generate_ticket_handler',
                'failure_text': None,
                'handler': None,
                'next_step': None
            }
        }
    }
}

DEFAULT_ANSWER = "Не знаю как на это ответить." \
                 "Могу сказать когда и где пройдет конференция, а также зарегистрировать вас. Просто спросите."