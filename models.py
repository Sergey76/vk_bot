from pony.orm import Database, Required, Json
import psycopg2

db = Database()
db.bind(provider='postgres', user='postgres', password='1', host='localhost',
        database='vk_chat_bot')

class UserState(db.Entity):
        """Состояние пользователя внутри сценария."""
        user_id = Required(str, unique=True)
        scenario_name = Required(str)
        step_name = Required(str)
        context = Required(Json)

class Registration(db.Entity):
        """Заявка на регистрацию"""
        name = Required(str)
        email = Required(str)

db.generate_mapping(create_tables=True)