from copy import deepcopy
from unittest import TestCase
from unittest.mock import patch, Mock
from generate_ticket import generate_ticket
from pony.orm import db_session, rollback

import handlers
from vk_api.bot_longpoll import VkBotMessageEvent

from bot import Bot


def isolate_db(test_func):
    def wrapper(*args, **kwargs):
        with db_session:
            test_func(*args, **kwargs)
            rollback()

    return wrapper


class Testt1(TestCase):
    ROW_EVENT = {'type': 'message_new',
                 'object': {
                     'message': {'date': 1598878209, 'from_id': 79373617, 'id': 45,
                                 'out': 0,
                                 'peer_id': 79373617,
                                 'text': 'd', 'conversation_message_id': 44,
                                 'fwd_messages': [],
                                 'important': False,
                                 'random_id': 0, 'attachments': [], 'is_hidden': False},
                     'client_info': {
                         'button_actions': ['text', 'vkpay', 'open_app', 'location',
                                            'open_link'],
                         'keyboard': True, 'inline_keyboard': True, 'carousel': False,
                         'lang_id': 0}},
                 'group_id': 197536856,
                 'event_id': '8c60419b2f11cc148516f4a1f6efd25cfe7b224b'}

    def test_run(self):
        count = 5
        events = [{}] * count
        long_poller_mock = Mock(return_value=events)
        long_poller_listen_mock = Mock()

        long_poller_listen_mock.listen = long_poller_mock
        with patch('bot.vk_api.VkApi'):
            with patch('bot.VkBotLongPoll', return_value=long_poller_listen_mock):
                bot = Bot('', '')
                bot.on_event = Mock()
                bot.send_image = Mock()
                bot.run()
                bot.on_event.assert_called()
                bot.on_event.asset_called_with({})
                assert bot.on_event.call_count == count

    INPUTS = [
        'Привет',
        'А когда',
        'где будет конференция',
        'Зарегистрируй меня',
        'Вениамин',
        'моя почта pavelgmail.com',
        'моя почта pavel@gmail.com']
    EXPECTED_OUTPUTS = [
        handlers.DEFAULT_ANSWER,
        handlers.INTENTS[0]['answer'],
        handlers.INTENTS[1]['answer'],
        handlers.SCENARIOS['registration']['steps']['step1']['text'],
        handlers.SCENARIOS['registration']['steps']['step2']['text'],
        handlers.SCENARIOS['registration']['steps']['step2']['failure_text'],
        handlers.SCENARIOS['registration']['steps']['step3']['text'].format(
            name='Вениамин', email='pavel@gmail.com')
    ]
    @isolate_db
    def test_run_ok(self):
        send_mock = Mock()
        api_mock = Mock()
        api_mock.messages.send = send_mock

        events = []
        for input_text in self.INPUTS:
            event = deepcopy(self.ROW_EVENT)
            event['object']['message']['text'] = input_text
            events.append(VkBotMessageEvent(event))
        long_poller_mock = Mock()
        long_poller_mock.listen = Mock(return_value=events)
        with patch('bot.VkBotLongPoll', return_value=long_poller_mock):
            bot = Bot('', '')
            bot.api = api_mock
            bot.send_image = Mock()
            bot.run()
        assert send_mock.call_count == len(self.INPUTS)

        real_outputs = []
        for call in send_mock.call_args_list:
            args, kwargs = call
            real_outputs.append(kwargs['message'])
        assert real_outputs == self.EXPECTED_OUTPUTS

    def test_generate_ticket(self):
        with open('dsfdsfsd@jdsa.png', 'rb') as avatar_file:
            avatar_mock = Mock()
            avatar_mock.content = avatar_file.read()

        with patch('requests.get', return_value=avatar_mock):
            ticket_file  = generate_ticket('serg', 'dsfdsfsd@jdsa.ru')
        with open('ticket-example.png', 'rb') as expected_file:
            expected_bytes = expected_file.read()
        assert ticket_file.read() == expected_bytes


